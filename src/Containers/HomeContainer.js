import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({
    ContainerBG: { backgroundColor: '#e5e5e5' },
    CardView: { marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff' },
    HeaderStyle: {
        ...Platform.select({
            android: {
                backgroundColor: '#ffffff',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
            },
            ios: {
                backgroundColor: '#ffffff',
                height: 45,
            }
        })
    }

})