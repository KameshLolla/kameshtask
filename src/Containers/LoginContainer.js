import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({
    ContainerBG: { backgroundColor: '#C82257' },
    CenterView: { alignItems: 'center', marginTop: '50%' },
    SubView_1: {
        height: Platform.OS === 'ios' ? 50 : 45,
        flexDirection: 'row', borderWidth: 0.3, borderColor: "white", alignItems: 'center', marginTop: 20, width: '75%', alignSelf: 'center'
    },
    SubText_1: { height: '100%', fontSize: 14, marginLeft: 10, width: '74%', color: 'white', },
    ButtonView: { width: '75%', alignItems: 'center', backgroundColor: 'white', alignSelf: 'center', height: 40, justifyContent: 'center', marginTop: 30 },
    ButtonText: { color: '#C82257', fontSize: 16, fontWeight: 'bold' },
    CardView: { marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff' },
    spinnerTextStyle: {
        color: '#FFF',
    },
})

// LENGTH_LONG: 5000,
//     LENGTH_SHORT: 2000,
//     LENGTH_MEDIUM: 3000,
//     OPACITY: 0.8,
//     POSITION_BOTTOM: "bottom",
//     POSITION_TOP: "top",
//     FADE_IN_DURATION: 750,
//     FADE_OUT_DURATION: 1000