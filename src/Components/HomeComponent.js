import React, { Component } from 'react';
import { Text, TextInput, View, StatusBar, TouchableHighlight, FlatList, Image, TouchableOpacity, ScrollView, SafeAreaView, Keyboard, BackHandler } from 'react-native';
import { Container, Input, Header, Left, List, ListItem, Body, Thumbnail, Content, Right } from 'native-base';
import HomeContainer from "../Containers/HomeContainer"
import Toast from 'react-native-easy-toast';

const names = [{
    name: 'Chandi Amma',
    image: 'https://lh3.googleusercontent.com/proxy/sLc8a_3GUeao5PqbThLs-r6NzJy4gMg-ROvWRLFaZnVyrcOblVo8WM_1u-T2mGpJ7PbfEeRqctvH9-jW2u1OJ2DV3emBqxbZxv0nz0EXYjT55eNmgmSqm-Ix61lpf2J8CzPv06vXhy9FRLkN'
},
{
    name: 'Sudhakar',
    image: 'https://i1.wp.com/www.wordzz.com/wp-content/uploads/2017/04/Lord-Balaji.jpg?fit=1024%2C768&ssl=1'
},
{
    name: 'Vijita',
    image: 'https://cdnaws.sharechat.com/20b2eb7b-2590-4ed0-92bd-f7e9809d6ba8-ff205f63-3879-47d7-896f-0f4444929717_compressed_40.jpg'
},
{
    name: 'Kamesh',
    image: 'https://i0.wp.com/www.iskm.international/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-05-at-01.15.46.jpeg?fit=691%2C960&ssl=1'
},
{
    name: 'Akhilesh',
    image: 'https://www.hindisoch.com/wp-content/uploads/2016/08/Lord-Krishna-Images-8.jpg'
},
{
    name: 'Mohan',
    image: 'https://i.pinimg.com/originals/58/35/5d/58355d1fe5a13fb345e888fea99f9301.jpg'
},
{
    name: 'Mahesh Babu',
    image: 'https://static.toiimg.com/photo/msid-73168078/73168078.jpg?488216'
},
{
    name: 'Victory Venkatesh',
    image: 'https://english.cdn.zeenews.com/sites/default/files/2019/04/30/783084-venkatesh-daggubati.jpg'
}]

export default class HomeComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    componentDidMount() {
        console.error = (error) => error.apply;
        console.disableYellowBox = true;
    }

    render() {
        return (
            <Container style={HomeContainer.ContainerBG}>
                <Header style={HomeContainer.HeaderStyle}>

                    <Text style={{ fontSize: 20, color: "black", marginRight: 'auto', marginLeft: 20 }}>Home</Text>

                </Header>
                <FlatList
                    style={{ marginBottom: 5 }}
                    data={names}
                    keyExtractor={(item, index) => index}
                    renderItem={({ item }) =>


                        <View style={HomeContainer.CardView}>
                            <Content>
                                <List>
                                    <ListItem avatar>
                                        <Left>
                                            <Thumbnail style={{ margin: 5 }} source={{ uri: item.image }} />
                                        </Left>
                                        <Text style={{ color: "black", fontSize: 16, marginLeft: 'auto', marginRight: 20 }}> {item.name} </Text>
                                    </ListItem>
                                </List>
                            </Content>

                        </View>}

                />
            </Container>
        )
    }
}